﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using KaspiBank.Model;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace KaspiBank.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class postsController : ControllerBase
    {
        // GET: api/<postsController>
        [HttpGet]
        public List<News> Get()
        {
            var News = new News();
            return News.ReturnAll();
        }

        [HttpGet("{root}")]
        public List<News> Get(int root)
        {
            var News = new News();
            News.Union(2, 9);
            return null;
        }


        [HttpGet("{startDate}/{endDate}")]
        public List<News> Get(string startDate, string endDate)
        {
            var news = new News();
            return news.searchByDate(Convert.ToDateTime(startDate), Convert.ToDateTime(endDate));
        }
    }
}

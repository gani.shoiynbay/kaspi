﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KaspiBank.Model;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace KaspiBank.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class searchController : ControllerBase
    {
        // GET api/<searchController>/5
        [HttpGet("{word}")]
        public List<News> Get(string word)
        {
            var news = new News();
            return news.searchByWord(word);
        }
    }
}

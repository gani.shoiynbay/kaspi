﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KaspiBank.Model
{
    public class Context : DbContext
    {
        public DbSet<News> Newss{ get; set; }
  
        public virtual async Task<List<News>> FindAll()
        {
            var result = await Newss.ToListAsync();
            return result;
        }

        public async Task SaveAll( Dictionary<string, string> showResult)
        {
            using (var dbcontext = new Context())
            {  
                foreach (string key in showResult.Keys)
                {
                    News news = new News();
                    news.Content = showResult[key];
                    news.Title = key;
                    Random gen = new Random();
                    int range = 5 * 365; //5 years          
                    DateTime randomDate = DateTime.Today.AddDays(-gen.Next(range));
                    news.PublishTime = randomDate;
                    var entry = dbcontext.Entry(news);
                    dbcontext.Add(news);
                    entry.State = EntityState.Added;
                }
                   var saveResult = await dbcontext.SaveChangesAsync();
            }
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
                optionsBuilder.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB;Integrated Security=True");
        }
    }
}

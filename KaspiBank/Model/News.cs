﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KaspiBank.Model
{
    public class News
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime PublishTime { get; set; }
        public int ConnectionBetweenTitles { get; set; }

        public string[] topten()
        {
            var topten = new string[10];
            using (var dbcontext = new Context())
            {
                var all = dbcontext.FindAll().Result;
                var allWords = "";
                var max = -1000000;
                var topword = "";
               
                foreach (News news in all)
                {
                    allWords = allWords + " " + news.Content;
                }
                allWords = allWords.Replace("  ", " ");
                string[] massiveOfWords = allWords.Split(" ");

                var dict = new Dictionary<string, int>();

                foreach (string word in massiveOfWords)
                {
                    if (dict.ContainsKey(word))    
                        dict[word] = dict[word] + 1;
                    else
                    {
                        if (word!="" && word!= "\n")
                        dict.Add(word, 1);

                    }
                }
                for (int i = 0; i < 10; i++)
                { 
                    foreach (string word in dict.Keys)
                    {
                        if (dict[word] > max)
                        {
                            max = dict[word];
                            topword = word;
                        }
                    }
                    topten[i] = topword;
                    max = -100000;
                    dict.Remove(topword);
                }

            }
            return topten;
        }
        public List<News> ReturnAll()
        {
            using (var dbcontext = new Context())
            {
                return dbcontext.FindAll().Result;
            }
        }
        public List<News> RootedNews(int Root)
        {
            return null;
        }

        private int root(int i)
        {
            var listOfNews = new List<News>();
            
            using (var dbcontext = new Context())
            {
                listOfNews = dbcontext.FindAll().Result;
            }
            int[] rootedMass = new int[listOfNews.Count+1];
            int count = 1;
            foreach (News news in listOfNews)
            {
                rootedMass[count] = news.ConnectionBetweenTitles;
                count++;
            }

            while (i != rootedMass[i])
                i = rootedMass[i];
            return i;
        }

        public Boolean connected(int i, int j)
        {
            return root(i) == root(j);
        }


        public void Union(int firstElement, int secondElement)
        {
            var listOfNews = new List<News>();

            using (var dbcontext = new Context())
            {
                listOfNews = dbcontext.FindAll().Result;
            }

            int i = root(firstElement);
            int j = root(secondElement);
            var updateNewsUnion = listOfNews.Where(x => x.ConnectionBetweenTitles == i).FirstOrDefault();
            updateNewsUnion.ConnectionBetweenTitles = j;
            update(updateNewsUnion);
        }

        public void update(News news)
        {
            using (var dbcontext = new Context())
            {
                var result =  dbcontext.Update(news);
                dbcontext.SaveChanges();
            }
        }

        public List<News> searchByWord(string word)
        {
            var listOfNews = new List<News>();

            using (var dbcontext = new Context())
            {
                var all = dbcontext.FindAll().Result;

                foreach (News news in all)
                {
                    if (news.Content.Contains(word))
                    {
                        listOfNews.Add(news);
                    }
                }
            }
           
                return listOfNews;
        }
        public List<News> searchByDate(DateTime startTime,  DateTime endTime)
        {
            var listOfNews = new List<News>();

            using (var dbcontext = new Context())
            {
                var all = dbcontext.FindAll().Result;

                foreach (News news in all)
                {
                    if (news.PublishTime>=startTime && news.PublishTime >=endTime)
                    {
                        listOfNews.Add(news);
                    }
                }
            }

            return listOfNews;
        }
    }
}

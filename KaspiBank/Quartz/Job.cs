﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Xml;
using Parser.Core;
using Parser.Habr;
using KaspiBank.Model;
using Microsoft.EntityFrameworkCore;
using KaspiBank.Utils;

namespace Quartz
{
    public class Job : IJob
    {

        public  IConfiguration configuration;

        public Job(IConfiguration configuration)
        {
           
            this.configuration = configuration;
        }
   
        public async Task Execute(IJobExecutionContext context)
        {  
            ParserWorker<Dictionary<List<string>, List<string>>> parser;

            parser = new ParserWorker<Dictionary<List<string>, List<string>>>(new HabreParser());

            parser.Setting = new HabreSettings(1, 1);
          
            var result = await parser.Start();
            var showResult = Utils.ReturnList(result);
            var finalDic = new Dictionary<List<string>, List<string>>();
     
            using (var dbcontext = new Context())
            {
                await dbcontext.SaveAll(showResult);
            }


                parser.Abort();
        }
    }
}


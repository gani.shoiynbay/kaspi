﻿using Microsoft.Extensions.DependencyInjection;
using Quartz.Spi;
using System;

namespace Quartz
{
    public class JobFactory : IJobFactory
    {
        protected readonly IServiceProvider Container;
        public JobFactory(IServiceProvider serviceProvider)
        {
            Container = serviceProvider;
        }

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            return Container.GetRequiredService(bundle.JobDetail.JobType) as IJob;
        }

        public void ReturnJob(IJob job) { (job as IDisposable)?.Dispose(); }
    }
}

﻿using Quartz.Impl;
using Quartz.Spi;
using System;

namespace Quartz
{
    public class QuartzStartup
    {
        private static IScheduler _scheduler; // after Start, and until shutdown completes, references the scheduler object        
        public static IServiceProvider _container;
        // starts the scheduler, defines the jobs and the triggers
        public QuartzStartup(IServiceProvider container)
        {
            _container = container;
        }
        public  void Start()
        {
            var jovFactory = _container.GetService(typeof(IJobFactory)) as IJobFactory;
            if (_scheduler != null)
            {
                throw new InvalidOperationException("Already started.");
            }

            var schedulerFactory = new StdSchedulerFactory();
            _scheduler = schedulerFactory.GetScheduler().Result;
            _scheduler.JobFactory = new JobFactory(_container);
            _scheduler.Start().Wait();

            var voteJob = JobBuilder.Create<Job>()
                .Build();
            var voteJobTrigger = TriggerBuilder.Create()
                .StartNow()
                //.StartAt(Convert.ToDateTime("16.12.2020 14:00:00 +00:00"))
                .WithSimpleSchedule(s => s
                    .WithIntervalInHours(24)
                    .RepeatForever())
                .Build();

            _scheduler.ScheduleJob(voteJob, voteJobTrigger).Wait();
        }

        // initiates shutdown of the scheduler, and waits until jobs exit gracefully (within allotted timeout)
        public  void Stop()
        {
            if (_scheduler == null)
            {
                return;
            }

            // give running jobs 30 sec (for example) to stop gracefully
            if (_scheduler.Shutdown(waitForJobsToComplete: true).Wait(30000))
            {
                _scheduler = null;
            }
            else
            {
                // jobs didn't exit in timely fashion - log a warning...
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using AngleSharp.Html.Dom;
using Parser.Core;
namespace Parser.Habr
{
    class HabreSettings : IParserSetting
    {
        
        public string BaseUrl { get; set; } = "https://habr.com/ru/";
        public string Prefix { get; set; } = "page{CurrentId}";
        public int StartPoint { get; set ; }
        public int EndPoint { get; set; }
       public HabreSettings(int StartPoint, int EndPoint)
        {
            this.StartPoint = StartPoint;
            this.EndPoint = EndPoint;
        }
    }
}

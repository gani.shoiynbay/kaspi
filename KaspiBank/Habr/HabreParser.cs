﻿using AngleSharp.Html.Dom;
using Parser.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parser.Habr
{
    class HabreParser : IParser<Dictionary<List<string>, List<string>>>
    {
        public Dictionary<List<string>, List<string>> parse(IHtmlDocument document)
        {
            var listItems = new List<string>();
            var listContent = new List<string>();
            var dictionary = new Dictionary<List<string>, List<string>>();
            var items = document.QuerySelectorAll("a").Where(item => item.ClassName != null && item.ClassName.Contains("post__title_link"));
            var contents = document.QuerySelectorAll("div").Where(item => item.ClassName != null && item.ClassName.Contains("post__text"));
            foreach (var item in items)
            {
                listItems.Add(item.TextContent);

            }
            foreach (var content in contents)
            {
                listContent.Add(content.TextContent);
            }
            dictionary.Add(listItems, listContent);
            return dictionary;
        }
    }
}

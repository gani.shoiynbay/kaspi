﻿using AngleSharp.Html.Parser;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Parser.Core
{
    class ParserWorker<T> where T : class
    {
        IParser<T> parser;
        IParserSetting parserSetting;
        HtmlLoader loader;
        bool isActive { get; set; }
        public IParser<T> Parser { get { return parser; } set { parser = value; } }

        public IParserSetting Setting { get { return parserSetting; } set { parserSetting = value; loader = new HtmlLoader(value); } }
        public ParserWorker(IParser<T> parser)
        {
            this.parser = parser; 
        }
        public ParserWorker(IParser<T> parser, IParserSetting parserSetting) : this(parser)
        {
            this.parserSetting = parserSetting;
        }

        public async Task<List<T>> Start()
        {
            isActive = true;
           return await Worker();
        }
        public void Abort()
        {
            isActive = false;
        }

        private async Task <List<T>> Worker()
        {
            List<T> list = new List<T>(); 
            for (int i = parserSetting.StartPoint; i <= parserSetting.EndPoint; i++)
            {
                if (!isActive)
                {
                   return null;
                }
                var source = await loader.GetSourseByPageId(i);
                var domParse = new HtmlParser();

                var document = await domParse.ParseDocumentAsync(source);
                var result = parser.parse(document);
                list.Add(result);
            }
            isActive = false;
            return  await Task.FromResult(list);
        }

    }
}

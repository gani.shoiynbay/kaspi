﻿using AngleSharp.Html.Dom;
using System;
using System.Collections.Generic;
using System.Text;

namespace Parser.Core
{
   public interface IParser<T> where T : class
    {
        T parse(IHtmlDocument document);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Parser.Core
{
    class HtmlLoader
    {
        readonly HttpClient httpClient;

        readonly string url;
        public HtmlLoader(IParserSetting parserSetting)
        {
            httpClient = new HttpClient();
            url = $"{parserSetting.BaseUrl}/{parserSetting.Prefix}";
        }

        public async Task<string> GetSourseByPageId(int id)
        {
            var currentUrl = url.Replace("{CurrentId}", id.ToString());
            var response =  httpClient.GetAsync(currentUrl).Result;
            string sourse = null; 
            if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                sourse = await response.Content.ReadAsStringAsync();
            }
            return sourse;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KaspiBank.Utils
{
    public class Utils
    {
        public static Dictionary<string, string> ReturnList(List<Dictionary<List<string>, List<string>>> result)
        {
            var showResult = new Dictionary<string, string>();
            foreach (Dictionary<List<string>, List<string>> dict in result)
            {
                var myKey = dict.FirstOrDefault().Key;

                var Value = dict[myKey];
                var index = 0;
                foreach (string s in myKey)
                {

                    showResult.Add(s, Value[index]);
                    index++;
                }
            }
            return showResult;
        }
    }
}
